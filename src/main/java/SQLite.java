import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.impl.xb.xsdschema.All;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SQLite {
    static Connection connection = null;

    private static void checkConnection() throws SQLException {
        if(connection==null || connection.isClosed()){
            try{
                Class.forName("org.sqlite.JDBC").newInstance();
                // create a database connection
                connection = DriverManager.getConnection("jdbc:sqlite:"+System.getProperty("user.home")+ File.separator+ "Desktop"+ File.separator+"Report.db");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void createDB() throws ClassNotFoundException {
        try{
            checkConnection();
            createKW();
        }catch (Exception e) {
            e.printStackTrace();
        } finally{
            try{
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e){
                System.err.println(e);
            }
        }
    }

    private static void createKW() throws SQLException {
        checkConnection();
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeou
        statement.executeUpdate("drop table if exists `KW`");
        statement.executeUpdate("CREATE TABLE `KW` ( `start` , `end` , `kw`)");
        statement.executeUpdate("INSERT INTO KW VALUES('', '','')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-07-31', '2016-08-06','KW32 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-08-07', '2016-08-13','KW33 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-08-14', '2016-08-20','KW34 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-08-21', '2016-08-27','KW35 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-08-28', '2016-09-03','KW36 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-09-04', '2016-09-10','KW37 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-09-11', '2016-09-17','KW38 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-09-18', '2016-09-24','KW39 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-09-25', '2016-10-01','KW40 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-10-02', '2016-10-08','KW41 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-10-09', '2016-10-15','KW42 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-10-16', '2016-10-22','KW43 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-10-23', '2016-10-29','KW44 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-10-30', '2016-11-05','KW45 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-11-06', '2016-11-12','KW46 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-11-13', '2016-11-19','KW47 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-11-20', '2016-11-26','KW48 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-11-27', '2016-12-03','KW49 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-12-04', '2016-12-10','KW50 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-12-11', '2016-12-17','KW51 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-12-18', '2016-12-24','KW52 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2016-12-25', '2016-12-31','KW53 2016')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-01-01', '2017-01-07','KW1 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-01-08', '2017-01-14','KW2 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-01-15', '2017-01-21','KW3 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-01-22', '2017-01-28','KW4 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-01-29', '2017-02-04','KW5 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-02-05', '2017-02-11','KW6 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-02-12', '2017-02-18','KW7 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-02-19', '2017-02-25','KW8 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-02-26', '2017-03-04','KW9 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-03-05', '2017-03-11','KW10 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-03-12', '2017-03-18','KW11 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-03-19', '2017-03-25','KW12 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-03-26', '2017-04-01','KW13 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-04-02', '2017-04-08','KW14 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-04-09', '2017-04-15','KW15 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-04-16', '2017-04-22','KW16 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-04-23', '2017-04-29','KW17 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-04-30', '2017-05-06','KW18 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-05-07', '2017-05-13','KW19 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-05-14', '2017-05-20','KW20 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-05-21', '2017-05-27','KW21 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-05-28', '2017-06-03','KW22 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-06-04', '2017-06-10','KW23 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-06-11', '2017-06-17','KW24 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-06-18', '2017-06-24','KW25 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-06-25', '2017-07-01','KW26 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-07-02', '2017-07-08','KW27 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-07-09', '2017-07-15','KW28 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-07-16', '2017-07-22','KW29 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-07-23', '2017-07-29','KW30 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-07-30', '2017-08-05','KW31 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-08-06', '2017-08-12','KW32 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-08-13', '2017-08-19','KW33 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-08-20', '2017-08-26','KW34 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-08-27', '2017-09-02','KW35 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-09-03', '2017-09-09','KW36 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-09-10', '2017-09-16','KW37 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-09-17', '2017-09-23','KW38 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-09-24', '2017-09-30','KW39 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-10-01', '2017-10-07','KW40 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-10-08', '2017-10-14','KW41 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-10-15', '2017-10-21','KW42 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-10-22', '2017-10-28','KW43 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-10-29', '2017-11-04','KW44 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-11-05', '2017-11-11','KW45 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-11-12', '2017-11-18','KW46 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-11-19', '2017-11-25','KW47 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-11-26', '2017-12-02','KW48 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-12-03', '2017-12-09','KW49 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-12-10', '2017-12-16','KW50 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-12-17', '2017-12-23','KW51 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-12-24', '2017-12-30','KW52 2017')");
        statement.executeUpdate("INSERT INTO KW VALUES('2017-12-31', '2018-01-06','KW53 2017')");
    }

    public static void addTables(String sheet, String filename, String tablename, int header, int cellamount, boolean usecellamount) throws SQLException, IOException, InvalidFormatException {
        checkConnection();
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.

        File file = new File(filename);
        Workbook wb = WorkbookFactory.create(file);
        Sheet sh = wb.getSheet(sheet);
        Row row = sh.getRow(1);

        int cells = row.getLastCellNum();
        if(usecellamount){
            cells = cellamount;
        }
        //get header elements and add them to DB
        String headerelements ="";
        for(int celli = 0; celli<cells;celli++){
            headerelements += "\'"+sh.getRow(header).getCell(celli).getStringCellValue().replaceAll("'","''").trim() + "\' string,";
        }
        statement.executeUpdate("drop table if exists "+tablename);
        statement.executeUpdate("create table "+tablename+" ("+ headerelements.substring(0,headerelements.length()-1)+")");

        //get each row and add elemnets to db
        for(int rowi = header+1; rowi <= sh.getLastRowNum();rowi++){
            String lineelements ="";
            for(int celli = 0; celli < cells;celli++){
                try{
                    lineelements += "\'"+sh.getRow(rowi).getCell(celli).getStringCellValue().replaceAll("'","''").trim() + "\',";
                }catch(IllegalStateException iee){
                    lineelements += "\'"+sh.getRow(rowi).getCell(celli).getNumericCellValue() + "\',";
                }catch(NullPointerException nee){
                    lineelements += "\'"+"" + "\',";
                }
            }
            //System.out.println(rowi+1 + ". insert into "+tablename+" values("+lineelements.substring(0,lineelements.length()-1)+")");
            statement.executeUpdate("insert into "+tablename+" values("+lineelements.substring(0,lineelements.length()-1)+")");
        }
    }

    public static void createViewForTestDesign() throws SQLException {
        checkConnection();
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.
        statement.executeUpdate("DROP VIEW if exists designmerged");
        statement.executeUpdate("CREATE VIEW designmerged AS SELECT d.* FROM design as d, designtrace as t WHERE d.Id = t.Id");
    }

    public static void createViewForTestFinalDesign() throws SQLException {
        checkConnection();
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.
        statement.executeUpdate("DROP VIEW if exists designfinal");
        statement.executeUpdate("CREATE VIEW designfinal AS SELECT d.*, k.KW as KW FROM designmerged as d, KW as k Where date(substr(replace(substr(d.`Created Date`,0,11),\".\",\"-\"),7)||'-'||substr(replace(substr(d.`Created Date`,0,11),\".\",\"-\"),4,2)||'-'||substr(replace(substr(d.`Created Date`,0,11),\".\",\"-\"),1,2)) between date(k.start) AND date(k.end)\n");
    }

    public static void createViewForDefectsSubmittedDate() throws SQLException {
        checkConnection();
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.
        statement.executeUpdate("DROP VIEW if exists defectsSubmitted");
        statement.executeUpdate("CREATE VIEW defectsSubmitted AS SELECT d.Id, d.Summary, d.`Submitted Date`, sk.KW as `Submitted KW`, d.`Closed Date`, d.Submitter, d.Severity, d.Priority, d.Status, d.`Blocked TC`, d.Type FROM defects as d, KW as sk WHERE date(substr(replace(substr(d.`Submitted Date`,0,11),\".\",\"-\"),7)||'-'||substr(replace(substr(d.`Submitted Date`,0,11),\".\",\"-\"),4,2)||'-'||substr(replace(substr(d.`Submitted Date`,0,11),\".\",\"-\"),1,2)) between date(sk.start) AND date(sk.end)\n");
    }

    public static void createViewForDefectsClosedDate() throws SQLException {
        checkConnection();
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.
        statement.executeUpdate("DROP VIEW if exists defectsfinal");
        statement.executeUpdate("CREATE view defectsfinal AS SELECT d.Id, d.Summary, d.`Submitted Date`, d.`Submitted KW`, d.`Closed Date`, ck.KW as `Closed KW`, d.Submitter, d.Severity, d.Priority, d.Status, d.`Blocked TC`, d.Type FROM defectsSubmitted as d, KW as ck WHERE date(substr(replace(substr(d.`Closed Date`,0,11),\".\",\"-\"),7)||'-'||substr(replace(substr(d.`Closed Date`,0,11),\".\",\"-\"),4,2)||'-'||substr(replace(substr(d.`Closed Date`,0,11),\".\",\"-\"),1,2)) between date(ck.start) AND date(ck.end) OR d.`Closed Date` = ck.KW");
    }

    public static void createViewForExecutionKW() throws SQLException {
        checkConnection();
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.
        statement.executeUpdate("DROP VIEW if exists executionkw");
        statement.executeUpdate("create view executionkw as SELECT e.Directory, e.ID, e.Name, e.`Test Log #`, e.`Executed End`, k.KW as `Executed End KW` , e.Status, e.`# of Associated Defects`, e.Neu, e.Zugewiesen, e.Fixed, e.Reopened, e.Closed, e.Zurückgewiesen FROM execution as e, KW as k WHERE date(substr(replace(substr(e.`Executed End`,0,11),\".\",\"-\"),7)||'-'||substr(replace(substr(e.`Executed End`,0,11),\".\",\"-\"),4,2)||'-'||substr(replace(substr(e.`Executed End`,0,11),\".\",\"-\"),1,2)) between date(k.start) AND date(k.end) OR e.`Executed End` = k.KW");
    }

    public static void createViewForExecutionDesign() throws SQLException {
        checkConnection();
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.
        statement.executeUpdate("DROP VIEW if exists executiondesign");
        statement.executeUpdate("create view executiondesign as Select Distinct e.Directory, d.Id, e.Name, max(e.`Test Log #`) as `Test Log # (Filtered)`, e.`Executed End`, e.`Executed End KW`, e.Status, e.`# of Associated Defects`, e.Neu, e.Zugewiesen, e.Fixed, e.Reopened, e.Closed, e.Zurückgewiesen From executionkw as e, designfinal as d WHERE e.`Test Log #` != '' AND e.Name = d.Name GROUP BY e.Directory, e.Name ORDER BY d.Id");
    }

    public static void createViewForExecutionFinal() throws SQLException {
        checkConnection();
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.
        statement.executeUpdate("DROP VIEW if exists executionfinal");
        statement.executeUpdate("create view executionfinal as Select Distinct e.Directory, e.Id, e.Name, e.`Test Log # (Filtered)`, e.`Executed End`, e.`Executed End KW`, e.Status, e.`# of Associated Defects`, e.Neu, e.Zugewiesen, e.Fixed, e.Reopened, e.Closed, e.Zurückgewiesen From executiondesign as e union Select Distinct e.Directory, d.Id, e.Name, e.`Test Log # (Filtered)`, e.`Executed End`, e.`Executed End KW`, e.Status, e.`# of Associated Defects`, e.Neu, e.Zugewiesen, e.Fixed, e.Reopened, e.Closed, e.Zurückgewiesen  From executiondesign as e, designfinal as d WHERE e.`Test Log # (Filtered)` = '' AND e.Name = d.Name  GROUP BY e.Directory, e.Name ORDER BY d.Id    \n");
    }

    public static void createExcel(){
        String filename = System.getProperty("user.home")+ File.separator+ "Desktop"+ File.separator+"ReweReportValues.xlsx";
        createExcelFile(filename);
        writeToFile(filename, "Defects", "Select * from defectsfinal");
        writeToFile(filename, "Design", "Select * from designfinal");
        writeToFile(filename, "Execution", "Select * from executionfinal");
        writeToFile(filename, "BugOverView", "Select \"Minor\" as Defect, count() as \"amount of Defects\", sum(`Blocked TC`) as \"Unique blocked TCs\" From 'defectsfinal' Where Severity = 'Minor' AND Status not like 'Closed' AND Type like 'Bug' union Select \"Major\" as Defect, count() as \"amount\", sum(`Blocked TC`) as \"Unique blocked TCs\" From 'defectsfinal' Where Severity = 'Major' AND Status not like 'Closed' AND Type like 'Bug'");

    }

    private static void createExcelFile(String filename){
        try {
            XSSFWorkbook wb = new XSSFWorkbook();
            FileOutputStream out = new FileOutputStream(new File(filename));
            wb.write(out);
            out.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private static void writeToFile(String filename, String sheetname, String query){
        try {
            FileInputStream fis = new FileInputStream(new File(filename));
            XSSFWorkbook wb = new XSSFWorkbook(fis);
            XSSFSheet s = wb.createSheet(sheetname);
            short rownumber = 0;
            XSSFRow row = s.createRow(rownumber);
            //build up conenction to sqlite
            checkConnection();
            Statement statement = connection.createStatement();
            //set header
            ResultSet rs = statement.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            int columns = rsmd.getColumnCount();
            for(int i = 0; i < columns;i++){
                row.createCell(i).setCellValue(rsmd.getColumnName(i+1));
            }
            //set rows
            while(rs.next()){
                rownumber++;
                row = s.createRow(rownumber);
                for(int i = 0; i < columns;i++){
                    row.createCell(i).setCellValue(rs.getString(i+1));
                }
            }
            fis.close();
            FileOutputStream out = new FileOutputStream(filename);
            wb.write(out);
            out.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void colectDataToCorrectGraph() throws SQLException {
        checkConnection();
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.
        ResultSet r = statement.executeQuery("Select kw from KW");
        //get all kalendarweeks
        List<String> weeks = new ArrayList<>();
        while(r.next()){
            if(!r.getString("kw").equals("")){
                weeks.add(r.getString("kw"));
            }
        }
        //
        statement.setQueryTimeout(30);  // set timeout to 30 sec.
        r = statement.executeQuery("Select distinct Id from executionfinal");
        //get all kalendarweeks
        List<String> tcs = new ArrayList<>();
        while(r.next()){
            tcs.add(r.getString("Id"));
        }
        System.out.println("TCs: " + tcs.size());

        int i2 = 0;
        List result = new ArrayList();
        for(String week : weeks){
            int passed = 0;
            int obsolet = 0;
            int blocked = 0;
            int incomplete = 0;
            int failedmajor = 0;
            int failedminor = 0;
            int failed = 0;
            for(String tc : tcs){
                i2++;
                String query = "Select e.* from executionfinal as e, kw as k Where e.Id = \""+tc+"\" and k.KW = \""+week+"\" and date(substr(replace(substr(e.`Executed End`,0,11),\".\",\"-\"),7)||'-'||substr(replace(substr(e.`Executed End`,0,11),\".\",\"-\"),4,2)||'-'||substr(replace(substr(e.`Executed End`,0,11),\".\",\"-\"),1,2)) < date(k.end) ORDER BY e.`Executed End` ASC Limit 2";
                System.out.println(i2 + ". " + query);
                r = statement.executeQuery(query);
                if(r.next()){
                    String tcONEweek = r.getString("Executed End KW");
                    String tcONEStatus = r.getString("Status").toLowerCase();
                    String tcTWOStatus = "";
                    if(r.next()){
                        tcTWOStatus = r.getString("Status").toLowerCase();
                    }
                    if(tcONEweek.equals(week)){
                        switch(tcONEStatus){
                            case "passed":
                                passed++;
                                break;
                            case "blocked":
                                blocked++;
                                break;
                            case "obsolet":
                                obsolet++;
                                break;
                            case "incomplete":
                                incomplete++;
                                break;
                            case "failed":
                                failed++;
                                break;
                        }

                        switch(tcTWOStatus){
                            case "passed":
                                passed--;
                                break;
                            case "blocked":
                                blocked--;
                                break;
                            case "obsolet":
                                obsolet--;
                                break;
                            case "incomplete":
                                incomplete--;
                                break;
                            case "failed":
                                failed--;
                                break;
                        }
                    }
                }
            }
            result.add(week);
            result.add(passed);
            result.add(failed);
            result.add(blocked);
            result.add(obsolet);
            result.add(incomplete);
            System.out.println(week + ";" + passed + ";" + failed + ";" + blocked + ";" + incomplete);
        }
        System.out.println("ResultSize: " + result.size());
        for(int i = 0; i < result.size(); i = i + 6){
            //                      week                passed                  failed              blocked                 obsolet             incomplete
            System.out.println(result.get(i) + ";" + result.get(i+1) + ";" + result.get(i+2) + ";" + result.get(i+3) + ";" + result.get(i+4) + ";" + result.get(i+5));
        }
    }

}
