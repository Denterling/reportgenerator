import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLInput;

public class Gui extends JFrame {
    JPanel contentpane;
    public static void main(String[] args){
        Gui g = new Gui();
    }

    public Gui(){
        super("Report Generator");
        setBounds(100,100,600,195);
        contentpane = new JPanel();
        setContentPane(contentpane);
        contentpane.setLayout(null);
        setResizable(false);

        String dir = "/Users/denterling/Downloads/Vivaldi/";
        String executionFile = "FFC 2.0-Test Execution and Defects-20170925.xls";
        String defectFile = "FFC 2.0-All defects-20170925.xls";
        String designFile = "FFC 2.0-All test cases-20170925.xls";
        String designtraceFile = "FFC 2.0 - Test Case Traceability Matrix.xlsx";

        final JTextField TestExecution = new JTextField(dir + executionFile);
        TestExecution.setBounds(6, 6, 285, 23);
        TestExecution.setToolTipText("ExampleFileName: FFC 2.0-Test Execution and Defects-20170922.xls");
        contentpane.add(TestExecution);
        JButton selectTestExecution = new JButton("Select Test Execution file");
        selectTestExecution.setBounds(321, 6, 253, 23);
        selectTestExecution.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TestExecution.setText(getFile(Gui.this));
            }
        });
        contentpane.add(selectTestExecution);
        JLabel spinner1 = new JLabel(new ImageIcon(this.getClass().getResource("/Spin.gif")));
        spinner1.setBounds(TestExecution.getX()+TestExecution.getWidth()+3, TestExecution.getY(), 26, 26);
        spinner1.setVisible(false);
        contentpane.add(spinner1);

        final JTextField TestDefects = new JTextField(dir + defectFile);
        TestDefects.setBounds(TestExecution.getX(), TestExecution.getY()+TestExecution.getHeight()+6, TestExecution.getWidth(), TestExecution.getHeight());
        TestDefects.setToolTipText("ExampleFileName: FFC 2.0-All defects-20170922.xls");
        contentpane.add(TestDefects);
        JButton selectTestDefects = new JButton("Select Test Defect file");
        selectTestDefects.setBounds(selectTestExecution.getX(), selectTestExecution.getY()+selectTestExecution.getHeight()+6, selectTestExecution.getWidth(), selectTestExecution.getHeight());
        selectTestDefects.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TestDefects.setText(getFile(Gui.this));
            }
        });
        contentpane.add(selectTestDefects);
        JLabel spinner2 = new JLabel(new ImageIcon(this.getClass().getResource("/Spin.gif")));
        spinner2.setBounds(TestDefects.getX()+TestDefects.getWidth()+3, TestDefects.getY(), 26, 26);
        spinner2.setVisible(false);
        contentpane.add(spinner2);
        
        final JTextField TestDesign = new JTextField(dir + designFile);
        TestDesign.setBounds(TestDefects.getX(), TestDefects.getY()+TestDefects.getHeight()+6, TestDefects.getWidth(), TestDefects.getHeight());
        TestDesign.setToolTipText("ExampleFileName: FFC 2.0-All test cases-20170922.xls");
        contentpane.add(TestDesign);
        JButton selectTestDesign = new JButton("Select Test Design file");
        selectTestDesign.setBounds(selectTestDefects.getX(), selectTestDefects.getY()+selectTestDefects.getHeight()+6, selectTestDefects.getWidth(), selectTestDefects.getHeight());
        selectTestDesign.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TestDesign.setText(getFile(Gui.this));
            }
        });
        contentpane.add(selectTestDesign);
        JLabel spinner3 = new JLabel(new ImageIcon(this.getClass().getResource("/Spin.gif")));
        spinner3.setBounds(TestDesign.getX()+TestDesign.getWidth()+3, TestDesign.getY(), 26, 26);
        spinner3.setVisible(false);
        contentpane.add(spinner3);

        final JTextField TestDesignTraceability = new JTextField(dir + designtraceFile );
        TestDesignTraceability.setBounds(TestDesign.getX(), TestDesign.getY()+TestDesign.getHeight()+6, TestDesign.getWidth(), TestDesign.getHeight());
        TestDesignTraceability.setToolTipText("ExampleFileName: FFC 2.0 - Test Case Traceability Matrix.xlsx");
        contentpane.add(TestDesignTraceability);
        JButton selectTestDesignTraceability = new JButton("Select Test Design Traceability file");
        selectTestDesignTraceability.setBounds(selectTestDesign.getX(), selectTestDesign.getY()+selectTestDesign.getHeight()+6, selectTestDesign.getWidth(), selectTestDesign.getHeight());
        selectTestDesignTraceability.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TestDesignTraceability.setText(getFile(Gui.this));
            }
        });
        contentpane.add(selectTestDesignTraceability);
        JLabel spinner4 = new JLabel(new ImageIcon(this.getClass().getResource("/Spin.gif")));
        spinner4.setBounds(TestDesignTraceability.getX()+TestDesignTraceability.getWidth()+3, TestDesignTraceability.getY(), 26, 26);
        spinner4.setVisible(false);
        contentpane.add(spinner4);

        JButton executeCalc = new JButton("Execute");
        executeCalc.setBounds(6, 122, 568, 23);
        executeCalc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread processing = new Thread(() ->{
                    try {
                        SQLite.createDB();
                        spinner1.setVisible(true);
                        SQLite.addTables("CL-83 LVS",TestExecution.getText(), "execution",1,0,false);
                        System.out.println("DoneExecutoin");
                        spinner1.setVisible(false);
                        spinner2.setVisible(true);
                        SQLite.addTables("Defects", TestDefects.getText(), "defects",0,0,false);
                        System.out.println("DoneDefects");
                        spinner2.setVisible(false);
                        spinner3.setVisible(true);
                        SQLite.addTables("Test cases", TestDesign.getText(), "design",0,0,false);
                        System.out.println("DoneDesign");
                        spinner3.setVisible(false);
                        spinner4.setVisible(true);
                        SQLite.addTables("Test Case Traceability Matrix", TestDesignTraceability.getText(), "designtrace",4,2,true);
                        System.out.println("DoneDesignTrace");
                        spinner4.setVisible(false);

                        SQLite.createViewForTestDesign();
                        System.out.println("DoneDesignMerge");
                        SQLite.createViewForTestFinalDesign();
                        System.out.println("DoneDesignFinal");
                        SQLite.createViewForDefectsSubmittedDate();
                        System.out.println("DoneDefectSubmitted");
                        SQLite.createViewForDefectsClosedDate();
                        System.out.println("DoneDefectFinal");
                        SQLite.createViewForExecutionKW();
                        System.out.println("DoneExecutionKW");
                        SQLite.createViewForExecutionFinal();
                        System.out.println("DoneExecutionFinal");

                        SQLite.createExcel();
                        System.out.println("ExcelFileCreated");
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                });
                processing.start();
            }
        });
        contentpane.add(executeCalc);

        setVisible(true);
    }

    private String getFile(JFrame parent){
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "XLS, XLSX, CSV", "xlsx", "xls", "csv");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(parent);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
           return chooser.getSelectedFile().getAbsolutePath();
        }
        return "";
    }
}